#Цель: Сравнить скорость вычисления функции разложением до бесконечной суммы и модулем math.
#Переменные: x(float)-значение x из диапазона сходимости (-1,1);
#mxI(int)-точность разложения функции до бесконечной суммы; cntCalc(int)-количество вычислений
#Метод: разложение в ряд Тейлора
#Дата написания: 04.12.2023.
#Программист: 

from time import perf_counter_ns
from math import log, e

#Ввод исходных данных
x = float(input('Введите значение x из диапазона сходимости (-1,1):'))
mxI = int(input('Введите точность разложения функции до бесконечной суммы:'))
cntCalc = int(input('Введите количество вычислений:'))

#Проверка
if -1<x<1 and mxI>1 and cntCalc>0:
    totalTime = 0
    totalTime2 = 0

    for k in range(0,cntCalc):

        startTime = perf_counter_ns() #Время начала замера

        #Разложение функции до бесконечной суммы
        s = 0
        degree = 1
        for i in range(1,mxI):
            degree = degree*x
            s = s - degree/i

        endTime = perf_counter_ns() #Время конца замера
        totalTime = totalTime + (endTime - startTime) #Накопление общего времени


        startTime2 = perf_counter_ns() #Время начала замера

        a = log(1-x,e)

        endTime2 = perf_counter_ns() #Время конца замера
        totalTime2 = totalTime2 + (endTime2 - startTime2)#Накопление общего времени

    #Вывод результатов
    print(f'Результат (разложение):{s}')
    print(f'Результат (math):{a}')
    print(f'Среднее время окончания (разложение):{totalTime/cntCalc} нс')
    print(f'Среднее время окончания (math):{totalTime2/cntCalc} нс')

else:
    exit('Ошибка в исходных данных.')
